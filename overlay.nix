final: prev: {
  haskellPackages = prev.haskellPackages.override (old: {
    overrides = prev.lib.composeExtensions (old.overrides or (_: _: {}))
    (hself: hsuper: {
      custom-taffybar = hself.callCabal2nix "custom-taffybar"
      (
        final.lib.sourceByRegex ./.
        ["taffybar.hs" "custom-taffybar.cabal"]
      )
      { };
    });
  });
}
